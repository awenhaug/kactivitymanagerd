# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kactivitymanagerd package.
#
# Kishore G <kishore96@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kactivitymanagerd\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-05 02:51+0000\n"
"PO-Revision-Date: 2023-05-14 15:32+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.1\n"

#: service/Activities.cpp:86
#, kde-format
msgid "Default"
msgstr "இயல்பிருப்பு"

#: service/plugins/globalshortcuts/GlobalShortcutsPlugin.cpp:29
#, kde-format
msgid "Activity Manager"
msgstr "செயல்பாடு மேலாளி"

#: service/plugins/globalshortcuts/GlobalShortcutsPlugin.cpp:70
#: service/plugins/globalshortcuts/GlobalShortcutsPlugin.cpp:106
#, kde-format
msgctxt "@action"
msgid "Switch to activity \"%1\""
msgstr "\"%1\" என்ற செயல்பாட்டிற்கு தாவு"

#: service/plugins/krunner/ActivityRunner.cpp:19
#, kde-format
msgctxt "KRunner keyword"
msgid "activity"
msgstr ""

#: service/plugins/krunner/ActivityRunner.cpp:67
#, kde-format
msgid "Switch to \"%1\""
msgstr "\"%1\" என்பதற்கு தாவு"
